import {createAll} from './users.data';

const clubs = createAll();

console.log('----EXO 3---', clubs);

// faire une fonction qui prend en argument 'clubs', un argument 'name' de type
// string et qui renvoi un tableau contenant les joueurs dont le nom est égale à
// la valeur de 'name'. On ajoutera à chaque joueur le nom de son club d'origine
// avec l'atribbut 'club'.
function arrayAllUsersByName(clubs, name) {
  return clubs.reduce((acc, club) => acc.concat(club.users)
      .filter((user) => user.firstName === name)
      .map((user) => {
        user.club = club.name;
        return user;
      }), []);
}
console.log(arrayAllUsersByName(clubs, 'Mohamed'));
