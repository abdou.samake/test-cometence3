import {createAll} from './users.data';

const clubs = createAll();

console.log('----EXO 7---', clubs);

// créer une fonction qui prend en paramètre la variable "clubs" et qui
// renvoi un array contenant des objets dont les attributs sont les valeurs des attributs
// des clubs et les valeurs la concaténation de tous les prénoms des joueurs

const example = [
  {
    Paris0: 'BernardBiancaMichel',
    13: 'BernardBiancaMichel',
  }, {
    Berlin0: 'JoeJetteRillette',
    19: 'JoeJetteRillette',
  },
];

console.log(example);
function createObjectsInArray(teams) {
  const myObject = {};
  return teams.map((team) => {
    Object.values(team).forEach((value) => {
      if (typeof value !== 'object') {
        return myObject[value] = team.users.reduce((acc, user) => acc = acc + user.firstName, '');
      }
    });
    return myObject;
  });
}
console.log(createObjectsInArray(clubs));
