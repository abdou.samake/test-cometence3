import {createAll} from './users.data';
const clubs = createAll();

console.log('----EXO 4---', clubs);

// Créer une fonction qui prend en paramètre la variable "clubs", une variable
// "car (boolean) et une variable age (number)
// qui récupères toutes les joueurs de chaque
// club dans un seul tableau, puis qui mets une majuscule à tous leurs
// noms et prénoms, puis qui ne garde que les joueurs qui ont moins que le paramètre age
// et qui correspondent à l'inverse du paramètre car
function capi(name) {
  return typeof name === 'string' ? name.charAt(0).toUpperCase() + name.slice(1) : '';
}
function capiAllNameUsers(clubs, car, age) {
  return clubs.reduce((acc, club) => acc.concat(club.users), [])
      .map((user) => {
        user.firstName = capi(user.firstName);
        user.lastName = capi(user.lastName);
        return user;
      })
      .filter((user) => user.age < age && user.car !== car);
}
console.log(capiAllNameUsers(clubs, true, 50));
