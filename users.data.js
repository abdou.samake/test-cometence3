const firstName = [
  'joe',
  'jane',
  'seb',
  'alina',
  'fabien',
  'merwan',
  'anna',
  'annah',
  'Fathma',
  'Mohamed',
  'Walid',
  'Josiane',
  undefined,
];
const lastName = [
  'Dupont',
  'Dupond',
  'Durand',
  'Kassir',
  'Dalachra',
  'Hussein',
  'Wartani',
  'Thoumsi',
  'Angello',
  undefined,
];

const cityName = ['Paris', 'Londres', 'Berlin', 'Milan', 'Beauvais', 'Grenoble'];

function createClubs() {
  const companies = [];
  for (let i = 0; i < createRandomNumber(3, 10); i++) {
    const citySize = createRandomNumber(25, 42);
    companies.push({
      name: getValue(cityName) + i,
      size: citySize,
      users: createPlayer(citySize),
    });
  }
  return companies;
}

function createRandomNumber(min, max) {
  return Math.floor(min + Math.random() * (max - min));
}

function getValue(array) {
  return array[createRandomNumber(0, array.length)];
}

function createPlayer(end) {
  const tab = [];

  for (let i = 0; i < end; i++) {
    tab.push({
      firstName: getValue(firstName),
      lastName: getValue(lastName),
      age: createRandomNumber(16, 42),
      car: !!createRandomNumber(0, 2),
    });
  }
  return tab;
}

export function createAll() {
  return createClubs();
}


