import {createAll} from './users.data';

const clubs = createAll();
console.log('----EXO 6---', clubs);

// créer une fonction calcul la moyenne d'age de tous les clubs individuellement
// et qui le présente sous la forme de l'objet ci-dessous

const example = {
  nomduClub1: 23,
  nomduClub2: 12,
  nomduClub3: 36,
};

console.log(example);
const object = {};
function calculAgeMean() {
  clubs.forEach((club) => object[club.name] = Math.round(club.users
      .reduce((acc, user) => acc = acc + user.age, 0) / club.size));
  return object;
}
console.log(calculAgeMean());
