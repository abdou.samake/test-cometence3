import {createAll} from './users.data';

const clubs = createAll();

console.log('----EXO 8---', clubs);


// faire une fonction qui prend en argument 'clubs' et qui le transforme pour
// créer un array composé d'objets dont les attributs sont
// le nom des clubs et les valeurs la concaténation des prénom et des noms
// joueurs de chaque club mis en lettres capitales
// (toutes les lettres en majuscules, pas juste la première lettre)
// ne pas oublier de traiter les cas où il y a des undefined
function capi(name) {
  return typeof name === 'string' ? name.charAt(0).toUpperCase() + name.slice(name.length) : '';
}
function createObectValueCapiAllName(teams) {
  const object = {};
  teams.forEach((team) => object[team.name] = team.users
      .reduce((acc, user) => acc = acc + capi(user.firstName) + capi(user.lastName), ''));
  return object;
}
console.log(createObectValueCapiAllName(clubs));
